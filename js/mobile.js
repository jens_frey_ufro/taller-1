function imgSlider(anything){
    document.querySelector('.servant').src = anything;
}
function changeColor(color){
    const sec = document.querySelector('.sec');
    sec.style.background=color;
}
function changeText(texto){
    document.querySelector('.descript').src = texto;
}
function menuToggler(){
    const toggleMenu = document.querySelector('.toggleMenu');
    const navigation = document.querySelector('.navigation');
    toggleMenu.classList.toggle('active');
    navigation.classList.toggle('active');
}